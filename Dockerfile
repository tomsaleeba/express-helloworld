FROM node:18-alpine

WORKDIR /app
ADD package* *.js ./
RUN npm ci

ENTRYPOINT ["node", "."]
