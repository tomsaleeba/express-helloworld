const os = require('os')
const express = require('express')
const app = express()
const port = parseInt(process.env.PORT) || 3000

const hostname = os.hostname()

app.get('/', (req, res) => {
  const now = Date.now()
  res.send(`Hello from ${hostname} ${now}`)
})

process.on('SIGINT', function() {
  console.log('Received SIGINT. Exiting...')
  process.exit(0)
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
