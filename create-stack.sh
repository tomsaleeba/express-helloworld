#!/usr/bin/env bash
set -euxo pipefail
cd "$(dirname "$0")"

stackName=blah-stack
buffer=$(mktemp)

set +x
echo "Checking if stack already exists. Ignore error messages like"
echo '  "Stack with id njs-b-lt-hs-slapi does not exist"'
echo "...because that's exactly the thing we're checking."
set -x
aws cloudformation describe-stacks --stack-name $stackName | cat
STACK_EXISTS=$?

CF_ARGS="--stack-name $stackName \
  --template-body file://$PWD/cloudformation.yml \
  --capabilities CAPABILITY_IAM"

if [ "$STACK_EXISTS" = 0 ]; then
  echo "Updating existing stack ..."
  aws cloudformation update-stack $CF_ARGS | cat
else
  echo "Creating new stack ..."
  aws cloudformation create-stack $CF_ARGS | cat
fi

while [[ "$(aws cloudformation describe-stacks \
  --output=text \
  --query "Stacks[].StackStatus" \
  --stack-name $stackName | tee "$buffer")" = *"PROGRESS"
]]; do
  sleep 10
  echo "Stack Status: $(cat "$buffer")"
  aws cloudformation describe-stack-events \
    --output=table \
    --query "StackEvents[].[LogicalResourceId,ResourceStatus,ResourceType,ResourceStatusReason]" \
    --stack-name $stackName \
    --max-items 15 | cat
done

